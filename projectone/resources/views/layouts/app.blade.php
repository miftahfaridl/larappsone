<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>@yield('title','Laravel Apps')</title>
  </head>
  <body>

    @php
      $no = 1;
    @endphp

    @foreach($tasks as $task)
        {{ $no }} - {{ $task }} <br>
        @php $no++ @endphp
    @endforeach

    <br>
    <li><a href="/">Home</a></li>
    <li><a href="/contact">Contact</a></li>
    <li><a href="/about">About Us</a></li>

    @yield('content')
  </body>
</html>
