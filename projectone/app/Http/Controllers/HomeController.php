<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $tasks = ['Code', 'Eat', 'Sleep'];
        return view('layouts.app', ['tasks' => $tasks]);
    }

    public function about()
    {
        $tasks = ['Code', 'Eat', 'Sleep'];
        return view('about', ['tasks' => $tasks]);
    }

    public function contact()
    {
        $tasks = ['Code', 'Eat', 'Sleep'];
        return view('contact', ['tasks' => $tasks]);
    }
}
